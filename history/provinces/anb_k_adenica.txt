#k_adenica
##d_adenica
###c_balgarton
792 = {		#Balgarton

	# Misc
	culture = adeanic
	religion = adenican_adean
	holding = castle_holding

	# History
}
2269 = {

    # Misc
    holding = city_holding

    # History

}
2270 = {

    # Misc
    holding = church_holding

    # History

}
2271 = {

    # Misc
    holding = none

    # History

}

###c_tiltwick
765 = {		#Tiltwick

	# Misc
	culture = adeanic
	religion = adenican_adean
	holding = castle_holding

	# History
}
2272 = {

    # Misc
    holding = church_holding

    # History

}
2273 = {

    # Misc
    holding = city_holding

    # History

}
2274 = {

    # Misc
    holding = none

    # History

}

###c_upcreek
764 = {		#Upcreek

	# Misc
	culture = adeanic
	religion = adenican_adean
	holding = castle_holding

	# History
}
2275 = {

    # Misc
    holding = church_holding

    # History

}

##d_falsemire
###c_mireleigh
762 = {		#Mireleigh

	# Misc
	culture = adeanic
	religion = adenican_adean
	holding = castle_holding

	# History
}
2461 = {

    # Misc
    holding = city_holding

    # History

}
2462 = {

    # Misc
    holding = none

    # History

}

###c_falseharbour
761 = {		#Falseharbour

	# Misc
	culture = adeanic
	religion = adenican_adean
	holding = castle_holding

	# History
}
2463 = {

    # Misc
    holding = church_holding

    # History

}
2464 = {

    # Misc
    holding = city_holding

    # History

}

###c_listfields
763 = {		#Listfields

	# Misc
	culture = adeanic
	religion = adenican_adean
	holding = castle_holding

	# History
}
2267 = {

    # Misc
    holding = city_holding

    # History

}
2268 = {

    # Misc
    holding = none

    # History

}

##d_rohibon
###c_rohibon
769 = {		#Rohibon

	# Misc
	culture = adeanic
	religion = adenican_adean
	holding = castle_holding

	# History
}
2251 = {

    # Misc
    holding = city_holding

    # History

}
2252 = {

    # Misc
    holding = church_holding

    # History

}
2253 = {

    # Misc
    holding = none

    # History

}

###c_gallopsway
768 = {		#Gallopsway

	# Misc
	culture = adeanic
	religion = adenican_adean
	holding = castle_holding

	# History
}
2248 = {

    # Misc
    holding = city_holding

    # History

}
2249 = {

    # Misc
    holding = church_holding

    # History

}
2250 = {

    # Misc
    holding = none

    # History

}

###c_horsemans_advance
770 = {		#Horseman's Advance

	# Misc
	culture = adeanic
	religion = adenican_adean
	holding = castle_holding

	# History
}
2247 = {

    # Misc
    holding = none

    # History

}

###c_halansar
771 = {		#Halansar

	# Misc
	culture = adeanic
	religion = adenican_adean
	holding = castle_holding

	# History
}
2254 = {

    # Misc
    holding = church_holding

    # History

}
2255 = {

    # Misc
    holding = none

    # History

}

##d_verteben
###c_verteben
2265 = {	#Verteben

	# Misc
	culture = adeanic
	religion = adenican_adean
	holding = castle_holding

	# History
}
782 = {

    # Misc
    holding = city_holding

    # History

}
2264 = {

    # Misc
    holding = none

    # History

}

###c_shieldrest
781 = {		#Shieldrest

	# Misc
	culture = stone_dwarvish
	religion = cult_of_balgar
	holding = castle_holding

	# History
	1000.1.1 = {
		special_building_slot = shieldrest_mines_01
		special_building = shieldrest_mines_01
	}
}
2266 = {

    # Misc
    holding = none

    # History

}

##d_taran_plains
###c_taranton
774 = {		#Taranton

	# Misc
	culture = adeanic
	religion = adenican_adean
	holding = castle_holding

	# History
}
2261 = {

    # Misc
    holding = church_holding

    # History

}
2262 = {

    # Misc
    holding = none

    # History

}
2263 = {

    # Misc
    holding = city_holding

    # History

}

###c_cantercurse
773 = {		#Cantercurse

	# Misc
	culture = adeanic
	religion = adenican_adean
	holding = castle_holding

	# History
}
2258 = {

    # Misc
    holding = city_holding

    # History

}
2259 = {

    # Misc
    holding = none

    # History

}
2260 = {

    # Misc
    holding = none

    # History

}

###c_banwick
772 = {		#Banwick

	# Misc
	culture = adeanic
	religion = adenican_adean
	holding = castle_holding

	# History
}
2256 = {

    # Misc
    holding = city_holding

    # History

}
2257 = {

    # Misc
    holding = none

    # History

}

##d_acengard
###c_acengard
760 = {		#Acengard

	# Misc
	culture = adeanic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2276 = {

    # Misc
    holding = church_holding

    # History

}
2277 = {

    # Misc
    holding = city_holding

    # History

}
2278 = {

    # Misc
    holding = none

    # History

}

###c_feldham
767 = {		#Feldham

	# Misc
	culture = adeanic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2283 = {

    # Misc
    holding = church_holding

    # History

}
2284 = {

    # Misc
    holding = none

    # History

}

###c_carlanhal
757 = {		#Carlanhal

	# Misc
	culture = adeanic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
756 = {		#Silvervord

	# Misc
	holding = city_holding

	# History
}
2279 = {

    # Misc
    holding = city_holding

    # History

}
2280 = {

    # Misc
    holding = church_holding

    # History

}
2281 = {

    # Misc
    holding = none

    # History

}
2282 = {

    # Misc
    holding = none

    # History

}

###c_badeben
759 = {		#Badeben

	# Misc
	culture = adeanic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2465 = {

    # Misc
    holding = none

    # History

}
2466 = {

    # Misc
    holding = city_holding

    # History

}
2467 = {

    # Misc
    holding = none

    # History

}

##d_valefort
###c_valefort
780 = {		#Valefort

    # Misc
    culture = adeanic
    religion = adenican_adean
	holding = castle_holding

    # History
}
2237 = {

    # Misc
    holding = none

    # History

}

###c_widor
776 = {		#Widor

    # Misc
    culture = adeanic
    religion = adenican_adean
	holding = castle_holding

    # History
}
2238 = {

    # Misc
    holding = city_holding

    # History

}
