#k_dameria
##d_damesear
###c_anbenncost
8 = {		#Castle Dameris
	
	# Misc
	culture = damerian
    religion = cult_of_the_dame
    holding = castle_holding
	
	# History
}
924 = {		#The Bilge

    # Misc
	holding = none

    # History

}
925 = {		#Elvendocks

    # Misc
	holding = city_holding

    # History

}
926 = {		#Temple District

    # Misc
	holding = church_holding

    # History

}
1048 = {	#Towerhill District

    # Misc
	holding = none

    # History

}
927 = {		#Dovemarket

    # Misc
	holding = none

    # History

}

###c_auraire
168 = {		#Auraire

    # Misc
    culture = damerian
    religion = cult_of_the_dame
    holding = castle_holding

    # History
}
1046 = {	#Erecen

    # Misc
	holding = none

    # History

}
928 = {		#Toothspier

    # Misc
	holding = city_holding

    # History

}

###c_damesteeth
936 = {		#Pointsfort

    # Misc
	culture = damerian
	religion = cult_of_the_dame
	holding = castle_holding

    # History

}
11 = {		#Middletooth
	
	# Misc
	holding = city_holding
	# History
}
935 = {		#Millmore

    # Misc
	holding = none

    # History

}

###c_moonmount
4 = {		#Temple of the Highest Moon
	
	# Misc
	culture = damerian
	religion = cult_of_the_dame
    holding = church_holding
    
    # History
}
1045 = {	#Melefcost

    # Misc
	holding = none

    # History

}
1044 = {	#Damesridge

    # Misc
	holding = none

    # History

}
1043 = {	#Cannionn

    # Misc
	holding = none

    # History

}
9 = {		#Moonisle
	
	# Misc
	holding = church_holding
	# History
	1000.1.1 = {
		special_building_slot = moonisle_mines_01
	}

}
931 = {		#Earshore

    # Misc
	holding = city_holding

    # History

}

###c_old_damenath
3 = {		#Old Damenath
	
	# Misc
	culture = damerian
    religion = cult_of_the_dame
    holding = city_holding
	
	# History
}
1047 = {	#Safeharbour

    # Misc
	holding = none

    # History

}
1 = {		#Torrenwood
	
	# Misc
	holding = none
	
	# History

}
938 = {		#Magepoint

    # Misc
	holding = castle_holding

    # History

}

###c_damerian_fields
6 = {		#Damerian Fields
	
	# Misc
	culture = damerian
	religion = cult_of_the_dame
	holding = castle_holding
	# History
}
929 = {		#Queensgrove

    # Misc
	holding = church_holding

    # History

}
930 = {		#Dameced

    # Misc
	holding = none

    # History

}

###c_varivar
5 = {		#Cymlan Docks
	
	# Misc
	culture = moon_elvish
	religion = cult_of_the_dame
	holding = city_holding
	# History
}
1041 = {	#Rialveren

    # Misc
	holding = castle_holding

    # History

}
932 = {		#Bluewood

    # Misc
	holding = none

    # History

}

###c_eargate
2 = {		#Eargate
	
	# Misc
	culture = damerian
	religion = cult_of_the_dame
	holding = castle_holding
	# History
}
1042 = {	#Estarog

    # Misc
	holding = church_holding

    # History

}
933 = {		#Marionaire

    # Misc
	holding = none

    # History

}
934 = {		#Floodtower Pass

    # Misc
	holding = none

    # History

}

##d_wesdam
###c_wesdam
10 = {		#Wesdam

    # Misc
    culture = lenco_damerian
    religion = cult_of_the_dame
    holding = castle_holding

    # History
}
28 = {		#Modgate

    # Misc
	holding = castle_holding

    # History

}
937 = {		#Trasenn

    # Misc
	holding = none

    # History

}

###c_toothsend
13 = {		#Toothsend

    # Misc
    culture = lenco_damerian
    religion = cult_of_the_dame
    holding = castle_holding

    # History

}
1012 = {	#Brightmeadow

    # Misc
	holding = none

    # History

}

###c_lenceiande
17 = {		#Lenceiande

    # Misc
    culture = lenco_damerian
    religion = cult_of_the_dame
    holding = castle_holding

    # History

}
1061 = {	#Lethmas

    # Misc
	holding = none

    # History

}
15 = {		#Gabelaire

    # Misc
	holding = city_holding

    # History

}

##d_neckcliffe
###c_neckcliffe
27 = {		#Neckcliffe

    # Misc
    culture = lenco_damerian
    religion = west_damish
    holding = castle_holding

    # History

}
26 = {		#Throatport

    # Misc
	holding = city_holding

    # History

}
941 = {		#Riverstead

    # Misc
	holding = none

    # History

}

###c_triancost
24 = {		#Stoneview

    # Misc
    culture = lenco_damerian
    religion = west_damish
    holding = castle_holding

    # History

}
1049 = {	#Greengrove

    # Misc
	holding = none

    # History

}
23 = {		#Triancost

    # Misc
	holding = city_holding

    # History

}
942 = {		#Oldstone

    # Misc
	holding = church_holding

    # History

}

###c_dockbridge
21 = {		#Dockbridge

    # Misc
    holding = castle_holding

    # History

}
1050 = {	#Westpier

    # Misc
	holding = city_holding

    # History

}
16 = {		#Windtower

    # Misc
    culture = lenco_damerian
    religion = west_damish
	holding = castle_holding

    # History

}

##d_exwes
###c_exwes_by_the_sea
154 = {		#Exwes by the Sea

    # Misc
    culture = exwesser
    religion = cult_of_the_dame
    holding = city_holding

    # History

}
1062 = {	#Wesserway

    # Misc
    culture = exwesser
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1063 = {	#Bittergrass

    # Misc
	holding = none

    # History

}

###c_exwes
153 = {		#Exwes

    # Misc
    culture = exwesser
    religion = cult_of_the_dame
    holding = castle_holding

    # History

}
939 = {		#Smallfort

    # Misc
	holding = city_holding

    # History

}

###c_hookfield
156 = {		#Hookfield

    # Misc
    culture = exwesser
    religion = cult_of_the_dame
    holding = castle_holding

    # History

}
18 = {		#Troutcoast

    # Misc
	holding = church_holding

    # History

}
940 = {		#Rodhill

    # Misc
	holding = none

    # History

}

##d_istralore
###c_istralore
277 = {		#Istralore

    # Misc
    culture = damerian
    religion = cult_of_the_dame

    # History
	1000.1.1 = {
		special_building_slot = damerian_dales_mines_01
		special_building = damerian_dales_mines_01
	}
}
1739 = {

    # Misc
	holding = church_holding

    # History

}
1740 = {

    # Misc
	holding = none

    # History

}
1741 = {

    # Misc
	holding = none

    # History

}
1748 = {

    # Misc
	holding = city_holding

    # History

}

###c_damesdale
37 = {		#Damesdale

    # Misc
    culture = damerian
    religion = cult_of_the_dame

    # History

}
1737 = {

    # Misc
	holding = none

    # History

}
1738 = {

    # Misc
	holding = city_holding

    # History

}

###c_ricancas
899 = {		#Ricancas

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1736 = {

    # Misc
	holding = church_holding

    # History

}

###c_yshwood
29 = {		#Yshwood

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1733 = {

    # Misc
	holding = city_holding

    # History

}
1734 = {

    # Misc
	holding = none

    # History

}
1735 = {

    # Misc
	holding = none

    # History

}

##d_silverwoods
###c_moonhaven
53 = {		#Moonhaven

    # Misc
    culture = moon_elvish
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
52 = {	#Port Munas

    # Misc
	holding = city_holding

    # History

}
1731 = {

    # Misc
	holding = none

    # History

}
1732 = {

    # Misc
	holding = church_holding

    # History

}

###c_dancers_retreat
54 = {		#Dancer's Retreat

    # Misc
    culture = moon_elvish
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1728 = {

    # Misc
	holding = city_holding

    # History

}
1729 = {

    # Misc
	holding = none

    # History

}

###c_riverglade
55 = {		#Riverglade

    # Misc
    culture = moon_elvish
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1730 = {

    # Misc
	holding = none

    # History

}

###c_silverwoods
56 = {		#Silverwoods

    # Misc
    culture = moon_elvish
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1726 = {

    # Misc
	holding = none

    # History

}

1727 = {

    # Misc
	holding = none

    # History

}

##d_acromton
###c_acromton
330 = {		#Acromton

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1742 = {

    # Misc
	holding = church_holding

    # History

}
1745 = {

    # Misc
	holding = none

    # History

}
1746 = {

    # Misc
	holding = city_holding

    # History

}
1747 = {

    # Misc
	holding = none

    # History

}

###c_damescross
276 = {		#Damescross

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1743 = {

    # Misc
	holding = castle_holding

    # History

}

1744 = {

    # Misc
	holding = none

    # History

}

##d_plumwall
###c_plumwall
281 = {		#Plumwall

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1752 = {

    # Misc
	holding = city_holding

    # History

}
1753 = {

    # Misc
	holding = none

    # History

}
1754 = {

    # Misc
	holding = castle_holding

    # History

}
1773 = {

    # Misc
	holding = church_holding

    # History

}

###c_taxwick
910 = {		#Taxwick

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1749 = {

    # Misc
	holding = city_holding

    # History

}
1750 = {

    # Misc
	holding = church_holding

    # History

}
1751 = {

    # Misc
	holding = none

    # History

}

###c_silvelar
1755 = {		#Silvelar

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = silvelar_mines_01
		special_building = silvelar_mines_01
	}
}
328 = {

    # Misc
	holding = city_holding

    # History

}
1756 = {

    # Misc
	holding = none

    # History

}
1757 = {

    # Misc
	holding = none

    # History

}

##d_upper_luna
###c_lancecastle
298 = {		#Lancecastle

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1769 = {

    # Misc
    holding = city_holding

    # History

}
1770 = {

    # Misc
    holding = castle_holding

    # History

}
1771 = {

    # Misc
	holding = none

    # History

}
1772 = {

    # Misc
	holding = none

    # History

}

###c_cestaire
327 = {		#Cestaire

    # Misc
    culture = old_damerian
    religion = luna_damish
	holding = castle_holding

    # History

}
1785 = {

    # Misc
	holding = city_holding

    # History

}

###c_aranthil
898 = {		#Aranthil

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1774 = {

    # Misc
	holding = none

    # History

}
1778 = {

    # Misc
	holding = city_holding

    # History

}

###c_westshields
299 = {		#Westshields

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1775 = {

    # Misc
    holding = church_holding

    # History

}
1776 = {

    # Misc
    holding = none

    # History

}
1777 = {

    # Misc
    holding = none

    # History

}

##d_heartlands
###c_dhaneir
1779 = {    #Bluehart

    # Misc
    culture = damerian
    religion = luna_damish
	holding = city_holding

    # History

}
1780 = {

    # Misc
    culture = damerian
    religion = luna_damish
	holding = castle_holding

    # History

}
1783 = {

    # Misc
	holding = none

    # History

}

###c_elensbridge
109 = {		#Elensbridge

    # Misc
    culture = old_damerian
    religion = luna_damish
	holding = castle_holding

    # History

}
1789 = {

    # Misc
	holding = none

    # History

}

###c_palemarket
278 = {		#Palemarket

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1758 = {

    # Misc
	holding = city_holding

    # History

}
1759 = {

    # Misc
	holding = church_holding

    # History

}
1760 = {

    # Misc
	holding = none

    # History

}

###c_eastbright
394 = {		#Eastbright

    # Misc
    culture = old_damerian
    religion = cult_of_the_dame
	holding = castle_holding

    # History

}
1761 = {

    # Misc
	holding = none

    # History

}
1762 = {

    # Misc
	holding = church_holding

    # History

}

###c_cannleigh
296 = {		#Cannleigh

    # Misc
    culture = old_damerian
    religion = luna_damish
	holding = castle_holding

    # History

}
1765 = {

    # Misc
	holding = church_holding

    # History

}
1766 = {

    # Misc
	holding = none

    # History

}

###c_tirufeld
1767 = {

    # Misc
    culture = damerian
    religion = luna_damish
	holding = castle_holding

    # History

}
1768 = {

    # Misc
	holding = none

    # History

}

###c_byshade
282 = {		#Byshade

    # Misc
    culture = old_damerian
    religion = luna_damish
	holding = castle_holding

    # History

}
1763 = {

    # Misc
	holding = city_holding

    # History

}
1764 = {

    # Misc
	holding = none

    # History

}
