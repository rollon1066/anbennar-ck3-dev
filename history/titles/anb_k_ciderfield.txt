k_ciderfield = {
	1000.1.1 = {
		change_development_level = 8
	}
}

d_pearview = {
	1000.1.1 = {
		holder = 46 #Frederic Peartree
	}
}

c_butterburn = {
	1000.1.1 = {
		change_development_level = 10
	}
	1000.1.1 = {
		holder = 46 #Frederic Peartree
	}
}

c_pearview = {
	1000.1.1 = {
		holder = 46 #Frederic Peartree
	}
}

c_newcobble = {
	1000.1.1 = {
		holder = 46 #Frederic Peartree
	}
}

d_appleton = {
	1000.1.1 = {
		holder = 45 #Jon Appleseed
	}
}

c_appleton = {
	1000.1.1 = {
		holder = 45 #Jon Appleseed
	}
}

c_cowskeep = {
	1000.1.1 = {
		holder = 45 #Jon Appleseed
	}
}

c_littlebrook = {
	1000.1.1 = {
		holder = 45 #Jon Appleseed
	}
}