﻿namespace = anb_religious_decision

# Corset Burials
anb_religious_decision.0001 = {
	type = character_event
	title = anb_religious_decision.0001.t
	desc = anb_religious_decision.0001.desc
	theme = faith
	override_background = {
		reference = wilderness 
	}

	left_portrait = {
		character = var:ancestor_to_bury
	}

	immediate = {
		add_piety = major_piety_value
		if = {
			limit = {
				any_vassal = {
					faith = {
						has_doctrine_parameter = corset_burials_active
					}
				}
			}
			every_vassal = {
				limit = {
					faith = {
						has_doctrine_parameter = corset_burials_active
					}
				}
				custom = give_sky_burial_vassals
				add_opinion = {
					modifier = pleased_opinion
					target = root
					opinion = 20
				}
			}
		}
	}

	option = {
		name = anb_religious_decision.0001.a
	}

	after = {
		remove_variable = ancestor_to_bury
	}
}

# Child of Jaher's line gains proper trait (fired on on_birth).
anb_religious_decision.0002 = {
	type = character_event
	hidden = yes
	
	trigger = {
		OR = {
			AND = {
				exists = mother
				mother = {
					OR = {
						has_trait = sun_reborn
						has_trait = sun_reborn_descendant
					}
				}
			}
			AND = {
				exists = father
				father = {
					OR = {
						has_trait = sun_reborn
						has_trait = sun_reborn_descendant
					}
				}
			}
		}
		NOR = {
			has_trait = sun_reborn # Just in case the tree was horribly tangled...
			has_trait = sun_reborn_descendant
		}
	}
	
	immediate = {
		add_trait = sun_reborn_descendant
		every_child = {
			trigger_event = anb_religious_decision.0002
		}
	}
}

#Skaldhyrric

anb_religious_decision.0501 = {
	type = character_event
	title = anb_religious_decision.0501.t
	desc = {
		desc = anb_religious_decision.0501.desc.beginning
	}
	theme = faith
	override_background = {
		reference = fp1_tribal_temple 
	}
	left_portrait = {
		character = root.realm_priest
		animation = personality_zealous
	}
	
	right_portrait = root

	# TODO - AI Chances

	option = { #Let Skald do it
		name = anb_religious_decision.0501.a

		trigger_event = anb_religious_decision.0502
	}
	option = { #Do it yourself
		name = anb_religious_decision.0501.b
		
		trigger_event = anb_religious_decision.0503
	}
	option = { #Don't do it
		name = anb_religious_decision.0501.c
		
		add_piety = 100
		remove_variable = reciting_skaldic_tales
	}
}

anb_religious_decision.0502 = { #Let Skald do it
	type = character_event
	title = anb_religious_decision.0502.t
	desc = {
		desc = anb_religious_decision.0502.desc.beginning
	}
	theme = faith
	override_background = {
		reference = fp1_tribal_temple 
	}
	left_portrait = {
		character = root.realm_priest
		animation = personality_zealous
	}

	after = {
		remove_variable = reciting_skaldic_tales
	}
	
	right_portrait = root

	option = { #Dirge of the Deep
		name = anb_religious_decision.0502.a
		
		add_piety = miniscule_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_dirge
			days = 5475
		}
	}
	option = { #Gjalund and the Giantslayers
		name = anb_religious_decision.0502.b
		
		add_piety = minor_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_gjalund
			days = 5475
		}
	}
	option = { #Beralic Saga
		name = anb_religious_decision.0502.c
		
		add_piety = minor_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_beralic
			days = 5475
		}
	}
	option = { #Voyage of the North Raider
		name = anb_religious_decision.0502.d
		
		add_piety = medium_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_voyage
			days = 5475
		}
	}
	option = { #Treasure of the Golden Forest
		name = anb_religious_decision.0502.e
		
		add_piety = medium_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_golden_forest
			days = 5475
		}
	}
	option = { #Master Boat Builders
		name = anb_religious_decision.0502.f
		
		add_piety = major_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_master_boatbuilders
			days = 5475
		}
	}
	option = { #Dragon and the Skald
		name = anb_religious_decision.0502.g
		
		add_piety = major_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_dragon_and_skald
			days = 5475
		}
	}
	option = { #Old Winter Lullaby
		name = anb_religious_decision.0502.h
		
		add_piety = major_piety_loss
		add_character_modifier = {
			modifier = skaldhyrric_old_winter_lullaby
			days = 5475
		}
	}
	option = { #Don't do it
		name = anb_religious_decision.0501.c
		
		add_piety = 100
	}
}

anb_religious_decision.0503 = { #Do it yourself
	type = character_event
	title = anb_religious_decision.0503.t
	desc = {
		desc = anb_religious_decision.0503.desc.beginning
	}
	theme = faith
	override_background = {
		reference = fp1_tribal_temple 
	}
	left_portrait = {
		character = root.realm_priest
		animation = personality_zealous
	}
	
	after = {
		remove_variable = reciting_skaldic_tales
	}

	right_portrait = root

	option = { #Dirge of the Deep
		name = anb_religious_decision.0503.a
		
		duel = {
			skill = learning
			value = mediocre_skill_rating 
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.a.success
				add_character_modifier = {
					modifier = skaldhyrric_dirge
					days = 5475
				}
				add_prestige = miniscule_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = miniscule_prestige_loss
			}
		}
	}
	option = { #Gjalund and the Giantslayers
		name = anb_religious_decision.0503.b
		
		duel = {
			skill = learning
			value = medium_skill_rating
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.b.success
				add_character_modifier = {
					modifier = skaldhyrric_gjalund
					days = 5475
				}
				add_prestige = minor_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = minor_prestige_loss
			}
		}
	}
	option = { #Beralic Saga
		name = anb_religious_decision.0503.c
		
		duel = {
			skill = learning
			value = medium_skill_rating
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.c.success
				add_character_modifier = {
					modifier = skaldhyrric_beralic
					days = 5475
				}
				add_prestige = minor_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = minor_prestige_loss
			}
		}
	}
	option = { #Voyage of the North Raider
		name = anb_religious_decision.0503.d
		
		duel = {
			skill = learning
			value = high_skill_rating
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.d.success
				add_character_modifier = {
					modifier = skaldhyrric_voyage
					days = 5475
				}
				add_prestige = medium_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = medium_prestige_loss
			}
		}
	}
	option = { #Treasure of the Golden Forest
		name = anb_religious_decision.0503.e
		
		duel = {
			skill = learning
			value = high_skill_rating
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.e.success
				add_character_modifier = {
					modifier = skaldhyrric_golden_forest
					days = 5475
				}
				add_prestige = medium_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = medium_prestige_loss
			}
		}
	}
	option = { #Master Boat Builders
		name = anb_religious_decision.0503.f
		
		duel = {
			skill = learning
			value = very_high_skill_rating
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.f.success
				add_character_modifier = {
					modifier = skaldhyrric_master_boatbuilders
					days = 5475
				}
				add_prestige = major_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = major_prestige_loss
			}
		}
	}
	option = { #Dragon and the Skald
		name = anb_religious_decision.0503.g
		
		duel = {
			skill = learning
			value = extremely_high_skill_rating
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.g.success
				add_character_modifier = {
					modifier = skaldhyrric_dragon_and_skald
					days = 5475
				}
				add_prestige = massive_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = massive_prestige_loss
			}
		}
	}
	option = { #Old Winter Lullaby
		name = anb_religious_decision.0503.h
		
		duel = {
			skill = learning
			value = extremely_high_skill_rating
			60 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = 3
				}
				desc = anb_religious_decision.0503.h.success
				add_character_modifier = {
					modifier = skaldhyrric_old_winter_lullaby
					days = 5475
				}
				add_prestige = massive_prestige_gain
			}
			40 = {
				compare_modifier = {
					value = scope:duel_value
					multiplier = -2
				}
				desc = anb_religious_decision.0503.failure
				add_prestige = massive_prestige_loss
			}
		}
	}
	option = { #Don't do it
		name = anb_religious_decision.0501.c
		
		add_piety = 100
	}
}